<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::post('login',[\App\Http\Controllers\UserController::class,'login'])->name('login');


//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
////    return $request->user();
//});

Route::middleware(['auth:sanctum','role:superAdmin'])->group(function () {
    Route::get('user',[\App\Http\Controllers\UserController::class,'user']);
    Route::get('role',[\App\Http\Controllers\UserController::class,'role']);
    Route::get('permission',[\App\Http\Controllers\UserController::class,'permission']);
});
