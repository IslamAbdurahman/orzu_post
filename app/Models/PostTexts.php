<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTexts extends Model
{
    use HasFactory;
    protected $table = 'post_texts';

    protected $fillable = [
        'post_id',
        'text',
    ];

    public function post(){return $this->belongsTo(Posts::class,'post_id','id');}

}
