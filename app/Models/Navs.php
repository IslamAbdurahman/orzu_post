<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Navs extends Model
{
    use HasFactory;

    protected $table = 'navs';

    protected $fillable = [
        'icon',
        'title',
        'order'
    ];

}
