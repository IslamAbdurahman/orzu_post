<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;
    protected $table = 'posts';

    protected $fillable = [
        'user_id',
        'tag_id',
        'cat_id',
        'nav_id',
        'image',
        'title',
        'plus',
        'minus',
        'info',
        'link',
    ];

    public function user(){return $this->belongsTo(User::class,'user_id','id');}
    public function tag(){return $this->belongsTo(Tags::class,'tag_id','id');}
    public function cat(){return $this->belongsTo(Categories::class,'cat_id','id');}
    public function nav(){return $this->belongsTo(Navs::class,'nav_id','id');}

}
