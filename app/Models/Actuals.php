<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actuals extends Model
{
    use HasFactory;
    protected $table = 'actuals';

    protected $fillable = [
        'text',
        'image',
        'title',
        'cat_id',
        'status'
    ];

    public function category(){return $this->belongsTo(Categories::class,'cat_id','id');}

}
