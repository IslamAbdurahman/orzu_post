<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function login(Request $request){
        try {
            $request->validate([
                'phone' => 'required',
                'password' => 'required'
            ]);

            $user = User::where('phone','=',$request->phone)->first();

            if ($user && Hash::check($request->password,$user->password)){
                $token = $user->createToken('Token')->plainTextToken;
                $user->token = $token;
                return response()->json([
                    'status'=>true,
                    'data'=>$user,
                    'message'=>'You are logged in.'
                ]);
            }else{
                return response()->json([
                    'status'=>false,
                    'data'=>'Phone or password incorrect',
                    'message'=>'Phone or password incorrect'
                ]);
            }
        }catch (\Exception $exception){
            return response()->json([
                'status'=>false,
                'data'=>'Login or password incorrect',
                'message'=>$exception->getMessage()
            ]);
        }

    }


    public function user(Request $request){
        try {
            if ($request->user()){
                $user = $request->user();

                return response()->json([
                    'status'=>true,
                    'data'=>$user,
                    'message'=>'User info'
                ]);
            }else{
                return response()->json([
                    'status'=>true,
                    'data'=>'',
                    'message'=>'You are logged in.'
                ]);
            }
        }catch (\Exception $exception){
            return response()->json([
                'status'=>false,
                'data'=>'',
                'message'=>'You are not logged in.'
            ]);
        }
    }



    public function role(Request $request){
        try {
            if ($request->user()){
                $user = $request->user()->roles;

                return response()->json([
                    'status'=>true,
                    'data'=>$user,
                    'message'=>'User info'
                ]);
            }else{
                return response()->json([
                    'status'=>true,
                    'data'=>'',
                    'message'=>'You are logged in.'
                ]);
            }
        }catch (\Exception $exception){
            return response()->json([
                'status'=>false,
                'data'=>'',
                'message'=>'You are not logged in.'
            ]);
        }
    }



    public function permission(Request $request){

        try {
//            abort_if(!$request->user()->can('admin.store'),true,'You don`t have permission');
            if ($request->user()){
                $user = $request->user()->permissions;

                return response()->json([
                    'status'=>true,
                    'data'=>$user,
                    'message'=>'User info'
                ]);
            }else{
                return response()->json([
                    'status'=>true,
                    'data'=>'',
                    'message'=>'You are logged in.'
                ]);
            }
        }catch (\Exception $exception){
            return response()->json([
                'status'=>false,
                'data'=>'',
                'message'=>$exception->getMessage()
            ]);
        }
    }
}
