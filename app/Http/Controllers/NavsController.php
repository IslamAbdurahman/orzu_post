<?php

namespace App\Http\Controllers;

use App\Models\Navs;
use Illuminate\Http\Request;

class NavsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Navs $navs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Navs $navs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Navs $navs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Navs $navs)
    {
        //
    }
}
