create table categories
(
    id   bigint auto_increment
        primary key,
    name varchar(255) null
);

create table actuals
(
    id         bigint auto_increment
        primary key,
    text       varchar(255)                          null,
    image      varchar(255)                          null,
    title      varchar(255)                          null,
    cat_id     bigint                                null,
    status     int                                   null,
    creted_at  timestamp default current_timestamp() null,
    updated_at timestamp default current_timestamp() null,
    constraint actuals_ibfk_1
        foreign key (cat_id) references categories (id)
            on update cascade
);

create index cat_id
    on actuals (cat_id);

create table failed_jobs
(
    id         bigint unsigned auto_increment
        primary key,
    uuid       varchar(255)                          not null,
    connection text                                  not null,
    queue      text                                  not null,
    payload    longtext                              not null,
    exception  longtext                              not null,
    failed_at  timestamp default current_timestamp() not null,
    constraint failed_jobs_uuid_unique
        unique (uuid)
)
    collate = utf8mb4_unicode_ci;

create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

create table navs
(
    id      bigint auto_increment
        primary key,
    icon    varchar(255) null,
    title   varchar(255) null,
    `order` int          null
);

create table password_reset_tokens
(
    email      varchar(255) not null
        primary key,
    token      varchar(255) not null,
    created_at timestamp    null
)
    collate = utf8mb4_unicode_ci;

create table permissions
(
    id         bigint unsigned auto_increment
        primary key,
    name       varchar(255) not null,
    guard_name varchar(255) not null,
    created_at timestamp    null,
    updated_at timestamp    null,
    constraint permissions_name_guard_name_unique
        unique (name, guard_name)
)
    collate = utf8mb4_unicode_ci;

create table model_has_permissions
(
    permission_id bigint unsigned not null,
    model_type    varchar(255)    not null,
    model_id      bigint unsigned not null,
    primary key (permission_id, model_id, model_type),
    constraint model_has_permissions_permission_id_foreign
        foreign key (permission_id) references permissions (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index model_has_permissions_model_id_model_type_index
    on model_has_permissions (model_id, model_type);

create table personal_access_tokens
(
    id             bigint unsigned auto_increment
        primary key,
    tokenable_type varchar(255)    not null,
    tokenable_id   bigint unsigned not null,
    name           varchar(255)    not null,
    token          varchar(64)     not null,
    abilities      text            null,
    last_used_at   timestamp       null,
    expires_at     timestamp       null,
    created_at     timestamp       null,
    updated_at     timestamp       null,
    constraint personal_access_tokens_token_unique
        unique (token)
)
    collate = utf8mb4_unicode_ci;

create index personal_access_tokens_tokenable_type_tokenable_id_index
    on personal_access_tokens (tokenable_type, tokenable_id);

create table roles
(
    id         bigint unsigned auto_increment
        primary key,
    name       varchar(255) not null,
    guard_name varchar(255) not null,
    created_at timestamp    null,
    updated_at timestamp    null,
    constraint roles_name_guard_name_unique
        unique (name, guard_name)
)
    collate = utf8mb4_unicode_ci;

create table model_has_roles
(
    role_id    bigint unsigned not null,
    model_type varchar(255)    not null,
    model_id   bigint unsigned not null,
    primary key (role_id, model_id, model_type),
    constraint model_has_roles_role_id_foreign
        foreign key (role_id) references roles (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index model_has_roles_model_id_model_type_index
    on model_has_roles (model_id, model_type);

create table role_has_permissions
(
    permission_id bigint unsigned not null,
    role_id       bigint unsigned not null,
    primary key (permission_id, role_id),
    constraint role_has_permissions_permission_id_foreign
        foreign key (permission_id) references permissions (id)
            on delete cascade,
    constraint role_has_permissions_role_id_foreign
        foreign key (role_id) references roles (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create table tags
(
    id   bigint auto_increment
        primary key,
    name tinyint(1) null
);

create table users
(
    id                bigint auto_increment
        primary key,
    name              varchar(255) not null,
    family            varchar(255) not null,
    email             varchar(255) not null,
    email_verified_at timestamp    null,
    phone             varchar(255) not null,
    password          varchar(255) not null,
    image             varchar(255) null,
    reg_date          timestamp    null,
    remember_token    varchar(100) null,
    created_at        timestamp    null,
    updated_at        timestamp    null,
    constraint users_email_unique
        unique (email),
    constraint users_phone_unique
        unique (phone)
)
    collate = utf8mb4_unicode_ci;

create table posts
(
    id         bigint auto_increment
        primary key,
    user_id    bigint                                null,
    tag_id     bigint                                null,
    cat_id     bigint                                null,
    nav_id     bigint                                null,
    image      varchar(255)                          null,
    title      varchar(255)                          null,
    plus       varchar(255)                          null,
    minus      varchar(255)                          null,
    info       varchar(255)                          null,
    link       varchar(255)                          null,
    created_at timestamp default current_timestamp() null,
    updated_at timestamp default current_timestamp() null,
    constraint posts_ibfk_1
        foreign key (user_id) references users (id)
            on update cascade,
    constraint posts_ibfk_2
        foreign key (tag_id) references tags (id)
            on update cascade,
    constraint posts_ibfk_3
        foreign key (cat_id) references categories (id)
            on update cascade,
    constraint posts_ibfk_4
        foreign key (nav_id) references navs (id)
            on update cascade
);

create table comments
(
    id         bigint auto_increment
        primary key,
    post_id    bigint                                null,
    user_id    bigint                                null,
    text       varchar(255)                          null,
    `rank`     int                                   null,
    creted_at  timestamp default current_timestamp() null,
    updated_at timestamp default current_timestamp() null,
    constraint comments_ibfk_1
        foreign key (post_id) references posts (id)
            on update cascade,
    constraint comments_ibfk_2
        foreign key (user_id) references users (id)
            on update cascade
);

create index post_id
    on comments (post_id);

create index user_id
    on comments (user_id);

create table post_images
(
    id      bigint auto_increment
        primary key,
    post_id bigint       null,
    image   varchar(255) null,
    constraint post_images_ibfk_1
        foreign key (post_id) references posts (id)
            on update cascade
);

create index post_id
    on post_images (post_id);

create table post_texts
(
    id      bigint auto_increment
        primary key,
    post_id bigint       null,
    text    varchar(255) null,
    constraint post_texts_ibfk_1
        foreign key (post_id) references posts (id)
            on update cascade
);

create index post_id
    on post_texts (post_id);

create index cat_id
    on posts (cat_id);

create index nav_id
    on posts (nav_id);

create index tag_id
    on posts (tag_id);

create index user_id
    on posts (user_id);

