<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdmin = User::create([
            'name'=>'superAdmin',
            'family'=>'superAdmin',
            'email'=>'superAdmin@gmail.com',
            'phone'=>'998911157709',
            'password'=>'$2y$10$dYvW5Z9qtSvea5jmKcxGdOOpZyBtRa27EjAKV/K.5SgHz3AZl1.Wm',
            'reg_date'=>date('Y-m-d H:i:s'),
        ]);

        $superAdmin->assignRole('superAdmin');


        $superAdmin = User::create([
            'name'=>'admin',
            'family'=>'admin',
            'email'=>'admin@gmail.com',
            'phone'=>'998331157709',
            'password'=>'$2y$10$dYvW5Z9qtSvea5jmKcxGdOOpZyBtRa27EjAKV/K.5SgHz3AZl1.Wm',
            'reg_date'=>date('Y-m-d H:i:s'),
        ]);

        $superAdmin->assignRole('admin');


        $superAdmin = User::create([
            'name'=>'manager',
            'family'=>'manager',
            'email'=>'manager@gmail.com',
            'phone'=>'998331157701',
            'password'=>'$2y$10$dYvW5Z9qtSvea5jmKcxGdOOpZyBtRa27EjAKV/K.5SgHz3AZl1.Wm',
            'reg_date'=>date('Y-m-d H:i:s'),
        ]);

        $superAdmin->assignRole('manager');


        $superAdmin = User::create([
            'name'=>'user',
            'family'=>'user',
            'email'=>'user@gmail.com',
            'phone'=>'998331157702',
            'password'=>'$2y$10$dYvW5Z9qtSvea5jmKcxGdOOpZyBtRa27EjAKV/K.5SgHz3AZl1.Wm',
            'reg_date'=>date('Y-m-d H:i:s'),
        ]);

        $superAdmin->assignRole('user');


    }
}
