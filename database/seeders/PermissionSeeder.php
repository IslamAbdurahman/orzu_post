<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // create permissions
        Permission::create(['name' => 'admin.store']);
        Permission::create(['name' => 'admin.update']);
        Permission::create(['name' => 'admin.destroy']);
        Permission::create(['name' => 'admin.index']);


        $user = User::find(1);
        $user->givePermissionTo(Permission::all());
    }
}
